<?php

/**
 * @file
 * Contains views access plugin for OG permissions
 */

/**
 * Allow views to allow access to only users with a specified permission in the
 * current group.
 */
class og_views_permissions_plugin_access_og_perm extends views_plugin_access {

  /**
   * Retrieve the options when this is a new access
   * control plugin
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['perm'] = array('default' => 'edit group');

    return $options;
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $perms = array();
    // Get list of permissions
    $module_info = system_get_info('module');
    foreach (og_get_permissions() as $perm => $info) {
      $module_name = $module_info[$info['module']]['name'];
      $perms[$module_name][$perm] = strip_tags($info['title']);
    }

    $form['perm'] = array(
      '#type' => 'select',
      '#options' => $perms,
      '#title' => t('OG permission'),
      '#default_value' => $this->options['perm'],
      '#description' => t('Only users with the selected permission flag in a group retrieved from context will be able to access this display.')
    );
  }

  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {

    $params = array(
      '@perm' => $this->options['perm'],
    );

    return t('@perm', $params);
  }

  /**
   * Determine if the current user has access or not.
   */
  function access($account) {
    return og_views_permissions_access($this->options['perm']);
  }

  /**
   * Determine the access callback and arguments.
   */
  function get_access_callback() {
    return array('og_views_permissions_access', array($this->options['perm']));
  }
}
