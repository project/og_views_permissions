<?php


/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_plugins().
 */
function og_views_permissions_views_plugins() {
  return array(
    'access' => array(
      'og_views_permissions' => array(
        'title' => t('OG Views Permissions'),
        'handler' => 'og_views_permissions_plugin_access_og_perm',
        'uses options' => TRUE,
      ),
    ),
  );
}